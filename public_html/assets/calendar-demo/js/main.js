

$(document).ready(function () {

  var $cal = $("#mini-cal-wrapper");

  $cal.on({
    click: function (e) {
      var $self = $(this);
      var url = $self.attr("href");

      var updatedUrl = url.replace(/\/([\w_\-0-9]+)\/month(\/\d+\/\d+)/, "\/$1\/mini_cal$2", url);

      $.ajax({
        url: updatedUrl,
        type: "GET",
        success: function (response) {
          $cal.html(response);
        },
      });

      e.preventDefault();
      e.stopPropagation();
      return false;
    },
  }, ".table thead a");
});
